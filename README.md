This repository serves as a decentralized identity proof connecting my OpenPGP key to a Codeberg (Forgejo) account.

For more details on OpenPGP identity proofs, visit [Keyoxide](https://docs.keyoxide.org/wiki/faq).
